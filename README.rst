Securing the Internet with the block chain ledger
=================================================

Introduction
------------

Since its inception, the Internet has been constrained by a difficult problem: 
Trust.

Its solution up until recently was "Because I said so" where "I" refers to an 
authority. But in a system designed to be distributed, authorities are points 
of weakness.

And the most crucial weaknesses for the security of the Internet are 
certification authorities and the domain name service authority, ICANN (the 
Internet Corporation for Assigned Names and Numbers).

The problem was solved in January 2009 by a person, or group of people, who 
refer to him / her / themselves as Satoshi Nakamoto. They solved it, not for 
SSL, or DNS, but for trade, in the form of a cryptocurrency called Bitcoin.

Instead of the "because I said so" solution, theirs is, "Because the vast 
majority says so."

The claim of the vast majority is recorded in the "block chain ledger", which 
is a distributed public database.


How it works
------------

The Transaction
~~~~~~~~~~~~~~~

.. figure:: https://bytebucket.org/kaapstorm/bsides_blockchain_2014/raw/46d7b1be5f2a21b0797e00e1841b379b4989afda/transactions.png?token=7478c8b139a1c3dca0aca432d231e249bcf553da
   :alt: Transactions
   :width: 733px
   :height: 434px
   :align: left

   Image credit: Satoshi Nakamoto (2009)

Imagine Pete Programmer buys a coffee from Carel Coffee-Shop. Pete Programmer 
is "Owner 1" in the image above, and Carel Coffee-Shop is "Owner 2". Carel 
gives Pete his public key, and Pete combines that with a hash of the previous 
transaction, and signs it with his private key. 

So now we know that Pete, the owner of the currency, has authenticated paying 
it to Carel.

But what is to prevent Pete from taking a pre-transaction backup, and trying to 
spend that same money on something else, either after he's used it to pay 
Carel, or before?

The answer is the block chain.


The Block Chain
~~~~~~~~~~~~~~~

Choose a time period. Make the calculation complex enough to approximately fit
the period. Reward nodes with Bitcoin and transaction fees. Broadcast the 
block. 

Because each block includes a hash of the block before it, it forms a chain. 
Trust the longest chain. It is the chain that has been worked on by the most 
nodes in the network; it is the consensus of the majority.

While it is possible to "attack" the network by lying, it becomes increasingly 
difficult to convince other nodes of your lie as time passes and chains grow in 
length -- your lying nodes have to catch up with the honest nodes who are 
collaborating against you.

Mining is now done with mining hardware to be more energy efficient, and blocks 
are created by pools of nodes. These pools share mined Bitcoin and transaction 
fees. 

(As long as no pool makes up more than 50% of the network, lying is 
impractical. As the network grows, a 50% pool becomes less and less likely.)

To watch transactions coming in, and blocks being built, take a look at 
`Blockchain.info <https://blockchain.info/>`_


Altcoins
~~~~~~~~

The Bitcoin idea can be applied to other problems. Altcoins cover both 
alternative cryptocurrencies, often invented to avoid the use of mining 
hardware (but in reality just delaying the use of mining hardware), and to 
apply the same idea to other problems.


Namecoin
~~~~~~~~

The problem I want to deal with is DNS, and the altcoin for that is Namecoin.

A Bitcoin transaction stores transaction data. Namecoin extends that for name 
registration data. Namecoin domains end in ".bit"

This solves a series of problems that result from placing control of DNS with a 
single authority and a handful of servers:

1. Censorship. A government can block those servers or DNS records, as the 
   Turkish government did with Twitter. In a peer-to-peer infrastructure, each 
   node is its own authority.

2. Security. HTTPS infrastructure can be compromised, either by a 
   certifying authority having its own credentials stolen, or by being forced 
   to issue fraudulent credentials. With decentralised, consensus-based 
   certification, those vulnerabilities are closed.

3. Lookups are much faster, because they are local. And updates are distributed 
   within 40 minutes on average, instead of the 24 - 48 hours in the case of 
   DNS.

4. Privacy is improved, slightly, by the fact that DNS lookups don't go over 
   the network.

It is important to note that Dot-Bit domains are not anonymous. The identity 
of the owner can be worked out by linking their Namecoin transactions.


Browsing the easy way
---------------------

Go to `FreeSpeechMe.org`_, and install the Firefox plugin. Or go to the Chrome 
Web Store, and look up the "dotbit.me .bit web surfer" extension.


.. _FreeSpeechMe.org: http://www.freespeechme.org/

Connecting to Dot-Bit hosts
---------------------------

The FreeSpeechMe plugin only works for websites. If you want to do anything 
other than web browsing, you need to set your computer's DNS servers to ones 
that support Dot-Bit domains -- which involves, once again, trusting an 
individual third party, something we are trying to avoid -- so, better yet, 
just run your own DNS server.


Third-party DNS server
^^^^^^^^^^^^^^^^^^^^^^

Your hosts entry should look something like this: ::

    178.32.31.41           dns.bit dns.dot-bit.org
    2001:41d0:2:f391::401  dns.bit dns.dot-bit.org


Your own DNS server
^^^^^^^^^^^^^^^^^^^

Install NMControl ::

    $ git clone https://github.com/namecoin/nmcontrol

Start namecoind as a daemon, and run NMControl ::

    $ namecoind -daemon
    $ cd nmcontrol
    $ sudo ./nmcontrol.py --daemon=0 start
    Plugins started : domain, guiHttp, guiHttpConfig, dns

Set your DNS server to 127.0.0.1 and ping a dot-bit domain: ::

    $ ping example.bit
    64 bytes from 192.168.11.1: icmp_seq=1 ttl=51 time=26.1 ms

DNSChain_ is like NMControl, but also allows you to query the namecoin database 
using the ".dns" top-level domain. Where NMControl is written in Python, 
DNSChain is written in CoffeeScript. Both read the database using namecoind's 
RPC API -- the same one you can get to from the command line. More on that 
later.


.. _DNSChain: http://okturtles.com/#DNSChain


How to register a .bit domain the easy way
------------------------------------------

Go to `dotbit.me`_ and follow their three-step wizard. But they own the domain.


.. _dotbit.me: https://dotbit.me/


How to register your own .bit domain properly
---------------------------------------------

First, `get a Namecoin client`_.  namecoind is a CLI client. namecoin-qt is a
GUI client. Packages are available for Linux and Windows, with OS X packages 
coming soon. The source is available on Github, and can be built on Linux, 
Windows and OS X. (See instructions below for building on a RaspberryPi running 
Raspbian.)

Then you wait while it downloads the block chain. (The GUI client calls it 
"synchronising with the network".) At the moment, the block chain is about 
1.8GB.

To register a domain you will need some currency. Go to an exchange like 
Kraken_, or Cryptonit_. Jump through some hoops, like sending them a photo of 
you with some form of ID and a utility bill, deposit some USD using PayPal, 
and buy some NMC. ... Or buy some on eBay_; it will be overpriced, but it might 
be quicker. Or mint them, slowly. Google "Bitcoin mining pools", but this is 
probably not the most efficient way to register a domain. Personally, I went 
with eBay and Cryptonit.

In the Namecoin client, receive your new NMC, and then you are ready to 
register.  You can do this using the GUI, or from the command line. 

If you want to use the command line, start the namecoind daemon, if you haven't
already. ::

    $ namecoind -daemon

There are three steps involved in registering a domain:

First, pre-order the domain. This costs 0.01 NMC registration fee (about $0.02 
USD at the moment) + standard transaction fee of 0.005 NMC. This reserves the 
name, but does not assign ownership to you! The command will return a short hex 
number and a long hex number. Make a note of the short number. Assuming you 
want to register the domain "example.bit", you would type ::

    $ namecoind name_new d/example

or click the "Manage Names" icon in the toolbar if you're using the GUI, fill 
"d/example" in the "New name" field, and click "Submit".

Interestingly, the registration fee doesn't go to anyone. It is lost.

Next, register the domain. It becomes public.  Currently this costs 0 NMC 
registration fee, but the price is variable, and may increase (check at 
http://dot-bit.org/tools/domainCost.php) + standard transaction fee. From this 
point you own the domain! ::

    $ namecoind name_firstupdate d/example <short hash> \
      '{"ip": "192.168.11.1", "email": "alice@example.com"}'

where "ip" is set to your server's IP address, and "email" is set to your 
e-mail address. Neither "ip" or "email" are mandatory settings, they're just 
examples. See the `Namecoin Wiki Domain Name Specification`_ for details of 
available settings.

An important setting to note is the "ns" setting. You can set it to a list of 
one or more name servers. If you do, this delegates IP-related responsibility
to the name servers, overriding your other settings, like "ip".

Using the GUI, select your new domain, and click the "Configure Name" button.

Each transaction expires after 36000 blocks. (One block ≈ 10 minutes, 36000 
blocks ≈ 250 days.) So, lastly, you'll want to update your details about every 
eight months to reset the expiry time to the maximum. No registration fee 
applies to updates, just the standard transaction fee. To update using the 
command line: ::

    $ namecoind name_update d/example \
      '{"ip": "192.168.11.1", "service": [["imap", "tcp", 0, 0, 143, "mail.host.com."]]}'

From the GUI, it is the same as your first update.


.. _get a Namecoin client: http://namecoin.info/?p=download
.. _Kraken: https://www.kraken.com/
.. _Cryptonit: https://www.cryptonit.net/
.. _eBay: http://www.ebay.com/
.. _Namecoin Wiki Domain Name Specification: https://wiki.namecoin.info/index.php?title=Domain_Name_Specification


How to read your Namecoin block chain programmatically
-----------------------------------------------------

If it isn't already running, start the namecoind daemon: ::

    $ namecoind -daemon

It comes with some help: ::

    $ namecoind help

And for the rest, poke things. See what happens. name_show and name_list are
useful commands. ::

    $ namecoind name_show d/example
    {
        "name": "d/example",
        "value": "{\"ip\": \"192.168.11.1\", \"email\": \"alice@example.com\"}"
        "txid": "1b2017cfbf34eca157b264ff28d433c5b9a4f97bb5f085384c8a073150bc",
        "address": "Mw83dvQkLnd3KA8i3h8kp7ZpS1QcE3",
        "expires_in": 35977
    }

But for pulling out details, I like NMControl. ::

    $ ./nmcontrol.py data getValue d/example
    {"ip": "192.168.11.1", "service": [["imap", "tcp", 0, 0, 143, "mail.host.com."]]}


It can do a lot more. Check out general help with, ::

    $ ./nmcontrol.py help

And the source is very readable. See what plugins are available and take a 
look at them.


What Next
---------

Hackers and programmers, the projects need eyes, and to be played with, and 
code. The more usable it is, the more it will be used.

Everyone, the more people who use Namecoin, the more credibility it gets for 
its solution to the problem of trust.


Postscript
----------

Compiling namecoind on a RaspberryPi: ::

    $ sudo apt-get install libboost-all-dev libssl-dev \
      libsqlite3-dev libdb++-dev libglib2.0-dev cmake
    $ git clone https://github.com/namecoin/namecoin.git
    $ cd namecoin/src
    $ make USE_UPNP=

(based on a combination of libcoin_ and `Namecoin build instructions`_)


.. _libcoin: https://github.com/libcoin/libcoin
.. _Namecoin build instructions: https://wiki.namecoin.info/index.php?title=Build_Namecoin_From_Source


References
----------

Bitcoin
~~~~~~~

`Enter The Blockchain: How Bitcoin Can Turn The Cloud Inside Out <http://techcrunch.com/2014/03/22/enter-the-blockchain-how-bitcoin-can-turn-the-cloud-inside-out/>`_ 

`Bitcoin <https://bitcoin.org/en/>`_

`Bitcoin: How it works <https://bitcoin.org/en/how-it-works>`_

`Nakamoto, Satoshi (24 May 2009). "Bitcoin: A Peer-to-Peer Electronic Cash System" <https://bitcoin.org/bitcoin.pdf>`_

`Bitcoin (Khan Academy) <https://www.khanacademy.org/economics-finance-domain/core-finance/money-and-banking/bitcoin/v/bitcoin-what-is-it>`_


Namecoin
~~~~~~~~

`Namecoin <http://namecoin.info/>`_

`Namecoin wiki <https://wiki.namecoin.info/index.php?title=Welcome>`_

`DNSChain <https://github.com/okTurtles/dnschain>`_

`XBTC Bitcoin Blog: SSL is Dead, Long Live the Blockchain <http://www.x-btc.com/1/post/2014/04/ssl-is-dead-long-live-the-blockchain.html>`_

`The Register: Getting rid of CAs <http://www.theregister.co.uk/2013/11/03/crypto_boffins_propose_getting_rid_of_cas/>`_


Other
~~~~~

`P2P Foundation <http://p2pfoundation.net/>`_


